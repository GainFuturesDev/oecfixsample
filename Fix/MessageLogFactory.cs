﻿using QuickFix;

namespace GF.Fix.Sample.Fix
{
    public sealed class MessageLogFactory : ILogFactory
    {
        public ILog Create() =>
            new MessageLog(null);

        public ILog Create(SessionID sessionID) =>
            new MessageLog(sessionID);
    }
}