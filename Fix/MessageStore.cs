﻿using System;
using GF.Fix.Sample.Property;
using QuickFix;

namespace GF.Fix.Sample.Fix
{
    internal class MessageStore : MessageStoreBase
    {
        public MessageStore(Props properties)
            : base(properties)
        {
        }

        protected override TimeSpan SessionStartLocal => Properties.SessionStart;

        protected override SessionParams LoadSessionParams(SessionID sessionID) =>
            new SessionParams
            {
                CreationTime = DateTime.UtcNow,
                SenderSeqNum = 1,
                TargetSeqNum = 1
            };

        protected override bool SaveSessionParams(SessionID sessionID, SessionParams sessionParams) =>
            true;
    }
}