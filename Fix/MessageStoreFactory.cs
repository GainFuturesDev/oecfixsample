﻿using GF.Fix.Sample.Property;
using QuickFix;

namespace GF.Fix.Sample.Fix
{
    internal sealed class MessageStoreFactory : IMessageStoreFactory
    {
        private readonly Props _properties;

        public MessageStoreFactory(Props properties)
        {
            _properties = properties;
        }

        public IMessageStore Create(SessionID sessionID)
        {
            var store = new MessageStore(_properties);
            store.Init(sessionID);
            return store;
        }
    }
}