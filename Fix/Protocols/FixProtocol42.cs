﻿using GF.Fix.Sample.Cfi;
using GF.Fix.Sample.FoxScript.Syntax;
using GF.Fix.Sample.Property;
using QuickFix;
using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Protocols
{
    internal class FixProtocol42 : FixProtocol
    {
        public override string BeginString => FixVersion.FIX42;

        public FixProtocol42(Props properties)
            : base(properties)
        {
        }

        public override void EnsureTrade(Message msg, char orderStatus, int? qty, double? price)
        {
            base.EnsureTrade(msg, orderStatus, qty, price);

            var execType = msg.GetChar<ExecType>();
            if (execType == null || execType.getValue() != ExecType.FILL)
            {
                throw new ExecutionException("Unexpected ExecType '{0}', must be '{1}'", execType, ExecType.FILL);
            }

            var execTransType = msg.GetChar<ExecTransType>();
            if (execTransType == null || execTransType.getValue() != ExecTransType.NEW)
            {
                throw new ExecutionException("Unexpected ExecTransType '{0}', must be '{1}'", execTransType, ExecTransType.NEW);
            }
        }

        public override void EnsureOrderStatus(Message msg, char orderStatus)
        {
            base.EnsureOrderStatus(msg, orderStatus);

            var execTransType = msg.GetChar<ExecTransType>();
            if (execTransType == null || execTransType.getValue() != ExecTransType.NEW)
            {
                throw new ExecutionException("Unexpected ExecTransType '{0}', must be '{1}'", execTransType, ExecTransType.NEW);
            }
        }

        public override void EnsureModifyAccepted(Message msg, char orderStatus)
        {
            EnsureOrderStatus(msg, orderStatus);
        }

        protected override void AssignOrderContract(FieldMap target, OrderContract orderContract)
        {
            base.AssignOrderContract(target, orderContract);

            if (target.IsSetField(Tags.CFICode))
            {
                string cfiCode = target.GetString(Tags.CFICode);
                target.RemoveField(Tags.CFICode);

                if (cfiCode == Code.Futures)
                    target.SetField(new SecurityType(SecurityType.FUTURE));
                else if (cfiCode == Code.FuturesMultileg || cfiCode == Code.FutureOptionsMultileg)
                    target.SetField(new SecurityType(SecurityType.MULTI_LEG_INSTRUMENT));
                else if (cfiCode == Code.FutureOptionsCall)
                {
                    target.SetField(new SecurityType(SecurityType.OPTIONS_ON_FUTURES));
                    target.SetField(new PutOrCall(PutOrCall.CALL));
                }
                else if (cfiCode == Code.FutureOptionsPut)
                {
                    target.SetField(new SecurityType(SecurityType.OPTIONS_ON_FUTURES));
                    target.SetField(new PutOrCall(PutOrCall.PUT));
                }
                else if (cfiCode == Code.Forex)
                    target.SetField(new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT));
                else
                    throw new ExecutionException("Unsupported CFICode '{0}'", cfiCode);
            }
        }
    }
}