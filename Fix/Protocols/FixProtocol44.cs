﻿using GF.Fix.Sample.Property;
using QuickFix;
using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Protocols
{
    internal class FixProtocol44 : FixProtocol
    {
        public override string BeginString => FixVersion.FIX44;

        public FixProtocol44(Props properties)
            : base(properties)
        {
        }

        public override void EnsureTrade(Message msg, char orderStatus, int? qty, double? price)
        {
            base.EnsureTrade(msg, orderStatus, qty, price);

            var execType = msg.GetChar<ExecType>();
            if (execType == null || execType.getValue() != ExecType.TRADE)
            {
                throw new ExecutionException("Unexpected ExecType '{0}', must be '{1}'", execType, ExecType.TRADE);
            }
        }
    }
}