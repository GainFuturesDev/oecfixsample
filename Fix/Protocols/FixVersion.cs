﻿using GF.Fix.Sample.Property;

namespace GF.Fix.Sample.Fix.Protocols
{
    internal static class FixVersion
    {
        public static readonly string FIX42 = "FIX.4.2";
        public static readonly string FIX44 = "FIX.4.4";

        public static FixProtocol Create(Props properties)
        {
            var current = properties.BeginString;
            if (current == FIX44)
                return new FixProtocol44(properties);
            if (current == FIX42)
                return new FixProtocol42(properties);

            throw new ExecutionException("Unsupported FIX version '{0}'", current ?? "NULL");
        }

        public static string Current(Props properties) =>
            properties.BeginString;
    }
}