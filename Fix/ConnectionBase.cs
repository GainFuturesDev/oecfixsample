﻿using GF.Fix.Sample.Fix.Fields;
using GF.Fix.Sample.Property;
using QuickFix;
using QuickFix.Fields;
using QuickFix.Transport;
using Tags = QuickFix.Fields.Tags;

namespace GF.Fix.Sample.Fix
{
    internal abstract class ConnectionBase : IApplication
    {
        private SocketInitiator _initiator;
        private MessageStoreFactory _messageStoreFactory;
        private string _password;
        private string _uuid;
        private Props _properties;

        public delegate void MessageHandler(SessionID sessionID, Message msg);
        public delegate void SessionEventHandler(SessionID sessionID);

        public event MessageHandler OnFromAdmin;
        public event MessageHandler OnFromApp;
        public event MessageHandler OnToAdmin;
        public event MessageHandler OnToApp;
        public event SessionEventHandler Logon;
        public event SessionEventHandler Logout;

        public SessionID SessionID => Session.SessionID;

        public Session Session { get; private set; }

        private int InitialSenderSeqNum => _properties.SenderSeqNum;

        private int InitialTargetSeqNum => _properties.TargetSeqNum;

        public void Create(Props properties)
        {
            _properties = properties;
            _uuid = _properties.UUID;

            _messageStoreFactory = new MessageStoreFactory(properties);
            var messageFactory = new DefaultMessageFactory();

            SessionSettings sessionSettings = GetSessionSettings();

            _initiator = new SocketInitiator(this, _messageStoreFactory, sessionSettings, new MessageLogFactory(), messageFactory);
        }

        public void Destroy()
        {
            if (_initiator != null)
            {
                _initiator.Stop();
                _initiator.Dispose();
                _initiator = null;
            }

            _messageStoreFactory = null;
        }

        public virtual void Open(string password, string uuid)
        {
            _password = password;

            if (!string.IsNullOrWhiteSpace(uuid))
                _uuid = uuid;

            _initiator.Start();
        }

        public void Close()
        {
            _initiator.Stop();
        }

        public void SendMessage(Message msg)
        {
            Session.SendToTarget(msg, SessionID);
        }

        protected abstract SessionSettings GetSessionSettings();

        #region Application Members

        public void FromAdmin(Message msg, SessionID sessionID)
        {
            OnFromAdmin?.Invoke(sessionID, msg);
        }

        public void FromApp(Message msg, SessionID sessionID)
        {
            OnFromApp?.Invoke(sessionID, msg);
        }

        public void OnCreate(SessionID sessionID)
        {
            Session = Session.LookupSession(sessionID);
            Session.NextSenderMsgSeqNum = InitialSenderSeqNum;
            Session.NextTargetMsgSeqNum = InitialTargetSeqNum;
        }

        public void OnLogon(SessionID sessionID)
        {
            Logon?.Invoke(sessionID);
        }

        public void OnLogout(SessionID sessionID)
        {
            Logout?.Invoke(sessionID);
        }

        public void ToAdmin(Message msg, SessionID sessionID)
        {
            if (msg.Header.GetString(Tags.MsgType) == MsgType.LOGON)
            {
                if (!string.IsNullOrEmpty(_password))
                {
                    msg.SetField(new Password(_password));
                }

                if (!string.IsNullOrEmpty(_uuid))
                {
                    msg.SetField(new UUIDField(_uuid));
                }
            }

            OnToAdmin?.Invoke(sessionID, msg);
        }

        public void ToApp(Message msg, SessionID sessionID)
        {
            OnToApp?.Invoke(sessionID, msg);
        }

        #endregion
    }
}