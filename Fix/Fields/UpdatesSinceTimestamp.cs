﻿using System;
using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    public class UpdatesSinceTimestamp : DateTimeField
    {
        public UpdatesSinceTimestamp(DateTime data)
            : base(Tags.UpdatesSinceTimestamp, data)
        {
        }

        public UpdatesSinceTimestamp()
            : base(Tags.UpdatesSinceTimestamp)
        {
        }
    }
}
