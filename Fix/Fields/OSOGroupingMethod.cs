﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    internal class OsoGroupingMethod : IntField
    {
        public const int FIELD = 12076;

        public const int ByFirstPrice = 0;
        public const int ByPrice = 1;
        public const int ByFill = 2;

        public OsoGroupingMethod(int value)
             : base(FIELD, value)
        {
        }

        public OsoGroupingMethod()
             : this(0)
        {
        }
    }
}
