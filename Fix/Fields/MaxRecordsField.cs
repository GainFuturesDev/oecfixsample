﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    public class MaxRecordsField : IntField
    {
        public MaxRecordsField(int value)
            : base(Tags.MaxRecordsField, value)
        {
        }

        public MaxRecordsField()
            : this(0)
        {
        }
    }
}