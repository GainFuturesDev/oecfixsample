﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    public class ByBaseContractsOnlyField : BooleanField
    {
        public ByBaseContractsOnlyField(bool value)
            : base(Tags.ByBaseContractsOnlyField, value)
        {
        }

        public ByBaseContractsOnlyField()
            : this(false)
        {
        }
    }
}