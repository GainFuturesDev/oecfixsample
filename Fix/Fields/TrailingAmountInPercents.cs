﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    public class TrailingAmountInPercents : BooleanField
    {
        public TrailingAmountInPercents()
            : this(false)
        {
        }

        public TrailingAmountInPercents(bool value)
            : base(Tags.TrailingAmountInPercents, value)
        {
        }
    }
}