﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    public class UUIDField : StringField
    {
        public UUIDField(string value)
            : base(Tags.UUIDField, value)
        {
        }

        public UUIDField()
            : this(string.Empty)
        {
        }
    }
}