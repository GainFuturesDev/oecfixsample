﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    internal class ClearingFirmID : StringField
    {
        public ClearingFirmID(string value)
            : base(Tags.ClearingFirmID, value)
        {
        }

        public ClearingFirmID()
            : base(Tags.ClearingFirmID)
        {
        }
    }
}