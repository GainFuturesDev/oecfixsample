﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    public class MaxQty : IntField
    {
        public MaxQty() : base(Tags.MaxQty)
        {
        }

        public MaxQty(int value) : base(Tags.MaxQty, value)
        {
        }
    }
}