﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    public class ContractGroupField : StringField
    {
        public ContractGroupField(string value)
            : base(Tags.ContractGroupField, value)
        {
        }

        public ContractGroupField()
            : this(string.Empty)
        {
        }
    }
}