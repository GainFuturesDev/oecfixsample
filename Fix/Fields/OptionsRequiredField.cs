﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.Fields
{
    public class OptionsRequiredField : BooleanField
    {
        public OptionsRequiredField(bool value)
            : base(Tags.OptionsRequiredField, value)
        {
        }

        public OptionsRequiredField()
            : this(false)
        {
        }
    }
}
