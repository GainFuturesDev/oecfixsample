﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.MarginCalc
{
    public sealed class MarginCalcReqResult : IntField
    {
        public enum Enum
        {
            Success,
            UnknownSession,
            UnknownAccount,
            EmptyRequest,
            InvalidContract,
            CalculationError,
            RequestExceedsLimit,
            MarginCalculatorDisabled
        }

        public const int FIELD = 12065;

        public MarginCalcReqResult(Enum value)
            : base(FIELD, (int)value)
        {
        }

        public MarginCalcReqResult()
            : this(0)
        {
        }
    }
}