﻿using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.MarginCalc
{
    public sealed class MarginCalcReqID : StringField
    {
        public const int FIELD = 12064;

        public MarginCalcReqID(string value)
            : base(FIELD, value)
        {
        }

        public MarginCalcReqID()
            : this(string.Empty)
        {
        }
    }
}