﻿using System;
using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.MarginCalc
{
    public sealed class RiskValue : DecimalField
    {
        public const int FIELD = 12070;

        public RiskValue(double value)
            : base(FIELD, Convert.ToDecimal(value))
        {
        }

        public RiskValue()
            : this(0)
        {
        }
    }
}