﻿using System;
using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.MarginCalc
{
    public sealed class NetOptionValue : DecimalField
    {
        public const int FIELD = 12069;

        public NetOptionValue(double value)
            : base(FIELD, Convert.ToDecimal(value))
        {
        }

        public NetOptionValue()
            : this(0)
        {
        }
    }
}