﻿using System;
using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.MarginCalc
{
    public sealed class InitialMargin : DecimalField
    {
        public const int FIELD = 12067;

        public InitialMargin(double value)
            : base(FIELD, Convert.ToDecimal(value))
        {
        }

        public InitialMargin()
            : this(0)
        {
        }
    }
}