﻿using QuickFix;
using QuickFix.Fields;
using Message = QuickFix.FIX44.Message;
using Tags = QuickFix.Fields.Tags;

namespace GF.Fix.Sample.Fix.MarginCalc
{
    public class MarginCalcRequest : Message
    {
        public const string MsgType = "UR";

        public MarginCalcRequest()
        {
            Header.SetField(new MsgType(MsgType));
        }

        public MarginCalcRequest(MarginCalcReqID reqID, Account account) : this()
        {
            SetField(reqID);
            SetField(account);
        }

        public class NoPositions : Group
        {
            private static readonly int[] MessageOrder =
            {
                Tags.Symbol, Tags.CFICode, Tags.MaturityMonthYear, Tags.StrikePrice, Tags.MinQty, Fields.Tags.MaxQty, 0
            };

            public NoPositions() : base(Tags.NoPositions, Tags.Symbol, MessageOrder)
            {
            }
        }
    }
}