﻿using System;
using QuickFix.Fields;

namespace GF.Fix.Sample.Fix.MarginCalc
{
    public sealed class MaintenanceMargin : DecimalField
    {
        public const int FIELD = 12068;

        public MaintenanceMargin(double value)
            : base(FIELD, Convert.ToDecimal(value))
        {
        }

        public MaintenanceMargin()
            : this(0)
        {
        }
    }
}