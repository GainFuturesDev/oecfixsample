﻿using System;
using QuickFix.Fields;
using QuickFix.FIX44;

namespace GF.Fix.Sample.Fix.MarginCalc
{
    public sealed class MarginCalcReportMessage : Message
    {
        private const string MsgType = "UM";

        public MarginCalcReportMessage(MarginCalcReqID reqID, Account account, MarginCalcReqResult marginCalcReqResult)
        {
            Header.SetField(new MsgType(MsgType));
            SetField(reqID);
            SetField(account);
            SetField(marginCalcReqResult);
            if (marginCalcReqResult.getValue() != 0)
                SetField(new Text(((MarginCalcReqResult.Enum)marginCalcReqResult.getValue()).ToString()));
        }

        internal void SetMarginValue(int field, double value)
        {
            if (double.IsNaN(value) || double.IsInfinity(value))
                return;

            SetField(new DecimalField(field, Convert.ToDecimal(value)));
        }
    }
}