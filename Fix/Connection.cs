﻿using System;
using System.IO;
using GF.Fix.Sample.Property;
using QuickFix;

namespace GF.Fix.Sample.Fix
{
    internal class Connection : ConnectionBase
    {
        private readonly Props _properties;

        public Connection(Props properties)
        {
            _properties = properties;
        }

        public void Create()
        {
            Create(_properties);
        }

        protected override SessionSettings GetSessionSettings()
        {
            var stream = new MemoryStream(1024);
            var writer = new StreamWriter(stream);

            writer.WriteLine("[DEFAULT]");
            writer.WriteLine("ConnectionType=initiator");
            writer.WriteLine($"HeartBtInt={_properties.HeartbeatInterval}");
            writer.WriteLine($"SocketConnectHost={_properties.Host}");
            writer.WriteLine($"SocketConnectPort={_properties.Port}");
            writer.WriteLine($"ReconnectInterval={_properties.ReconnectInterval}");
            writer.WriteLine("UseDataDictionary=N");
            writer.WriteLine($"MillisecondsInTimestamp={(_properties.MillisecondsInTimestamp ? "Y" : "N")}");
            writer.WriteLine("[SESSION]");
            writer.WriteLine($"BeginString={_properties.BeginString}");
            writer.WriteLine($"SenderCompID={_properties.SenderCompID}");
            writer.WriteLine($"TargetCompID={_properties.TargetCompID}");
            writer.WriteLine($"LogonTimeout={_properties.LogonTimeout}");
            writer.WriteLine($"StartTime={Tools.LocalToUtc(_properties.SessionStart)}");
            writer.WriteLine($"EndTime={Tools.LocalToUtc(_properties.SessionEnd)}");

            if (_properties.Contains(Prop.ResetSeqNumbers))
            {
                var resetSeqNums = _properties.ResetSeqNumbers ? "Y" : "N";
                writer.WriteLine($"ResetOnLogout={resetSeqNums}");
                writer.WriteLine($"ResetOnDisconnect={resetSeqNums}");
            }

            writer.WriteLine($"SSLEnable={(_properties.Contains(Prop.SSL) && _properties.SSL ? "Y" : "N")}");
            writer.WriteLine("SSLServerName=gainfutures.com");
            writer.WriteLine("SSLValidateCertificates=N");

            writer.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            return new SessionSettings(new StreamReader(stream));
        }
    }
}