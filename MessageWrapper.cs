﻿using GF.Fix.Sample.Fast;
using GF.Fix.Sample.Fix;
using QuickFix;

namespace GF.Fix.Sample
{
    internal class MessageWrapper
    {
        public readonly Message QFMessage;
        public readonly OpenFAST.Message OFMessage;

        private MessageWrapper(Message message)
        {
            QFMessage = message;
        }

        private MessageWrapper(OpenFAST.Message message)
        {
            OFMessage = message;
        }

        public bool IsQuickFix => QFMessage != null;

        public static MessageWrapper Create(Message message) =>
            message == null
                ? null
                : new MessageWrapper(message);

        public static MessageWrapper Create(OpenFAST.Message message) =>
            message == null
                ? null :
                new MessageWrapper(message);

        public static void SetFieldValue(MessageWrapper message, string fieldName, object fieldValue)
        {
            if (message == null)
                return;

            if (message.QFMessage != null)
                QFReflector.SetFieldValue(message.QFMessage, fieldName, fieldValue);
            else
                OFReflector.SetFieldValue(message.OFMessage, fieldName, fieldValue);
        }

        public static string FormatMessage(MessageWrapper message)
        {
            if (message == null)
                return "NULL";

            return message.QFMessage != null
                ? QFReflector.FormatMessage(message.QFMessage)
                : message.OFMessage.ToString();
        }

        public static object GetFieldValue(MessageWrapper message, string fieldName)
        {
            if (message == null) throw new ExecutionException("FIX message not specified.");

            return message.QFMessage != null
                ? QFReflector.GetFieldValue(message.QFMessage, fieldName)
                : OFReflector.GetFieldValue(message.OFMessage, fieldName);
        }
    }
}