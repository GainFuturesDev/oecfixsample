using System;
using GF.Fix.Sample.Fix.Protocols;

namespace GF.Fix.Sample.Configurations
{
    internal class ConfigurationParameters
    {
        public string Host { get; set; }

        public string SenderCompID { get; set; }

        public string TargetCompID { get; set; }

        public string UUID { get; set; } = "9e61a8bc-0a31-4542-ad85-33ebab0e4e86";

        public string FutureAccount { get; set; }

        public string ForexAccount { get; set; }

        public bool IsSSL { get; set; }

        ///default password
        public string Password { get; set; }

        public TimeSpan HeartBeatInterval { get; set; } = TimeSpan.FromSeconds(30);

        public bool ResetSeqNums { get; set; } = true;

        public int Port { get; set; } = 9300;

        public string BeginString { get; set; } = FixVersion.FIX44;
    }
}