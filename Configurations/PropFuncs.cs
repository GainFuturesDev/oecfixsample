using System;
using System.Collections.Specialized;
using GF.Fix.Sample.Property;

namespace GF.Fix.Sample.Configurations
{
    internal static class PropFuncs
    {
        public static Props ToProps(ConfigurationParameters configParameters) =>
            new Props
            {
                Host = configParameters.Host,
                Port = configParameters.Port + (configParameters.IsSSL ? 100 : 0),
                FastPort = configParameters.Port + 1,
                FastHashCode = "",
                ReconnectInterval = 30,
                HeartbeatInterval = (int)configParameters.HeartBeatInterval.TotalSeconds,
                MillisecondsInTimestamp = false,
                BeginString = configParameters.BeginString,
                SenderCompID = configParameters.SenderCompID,
                TargetCompID = configParameters.TargetCompID,
                SessionStart = new TimeSpan(1, 0, 0),
                SessionEnd = new TimeSpan(23, 0, 0),
                ResponseTimeout = TimeSpan.FromSeconds(15),
                ConnectTimeout = TimeSpan.FromSeconds(15),
                LogonTimeout = 30,
                SenderSeqNum = 1,
                TargetSeqNum = 1,
                FutureAccount = configParameters.FutureAccount,
                ForexAccount = configParameters.ForexAccount,
                SSL = configParameters.IsSSL,
                ResetSeqNumbers = configParameters.ResetSeqNums,
                Password = configParameters.Password,
                UUID = configParameters.UUID
            };

        public static Props ToProps(NameValueCollection settings) =>
            new Props
            {
                Host = settings[Prop.Host],
                Port = ToInt(settings, Prop.Port),
                FastPort = ToInt(settings, Prop.FastPort),
                FastHashCode = settings[Prop.FastHashCode],
                ReconnectInterval = ToInt(settings, Prop.ReconnectInterval),
                HeartbeatInterval = ToInt(settings, Prop.HeartbeatInterval),
                MillisecondsInTimestamp = ToBool(settings, Prop.MillisecondsInTimestamp),
                BeginString = settings[Prop.BeginString],
                SenderCompID = settings[Prop.SenderCompID],
                TargetCompID = settings[Prop.TargetCompID],
                LogonTimeout = ToInt(settings, Prop.LogonTimeout),
                SenderSeqNum = ToInt(settings, Prop.SenderSeqNum),
                TargetSeqNum = ToInt(settings, Prop.TargetSeqNum),
                FutureAccount = settings[Prop.FutureAccount],
                ForexAccount = settings[Prop.ForexAccount],
                SSL = ToBool(settings, Prop.SSL),
                ResetSeqNumbers = ToBool(settings, Prop.ResetSeqNumbers),
                UUID = "9e61a8bc-0a31-4542-ad85-33ebab0e4e86"
            };

        private static int ToInt(NameValueCollection settings, string keyName) =>
            int.Parse(settings[keyName]);

        private static bool ToBool(NameValueCollection settings, string keyName) =>
            bool.Parse(settings[keyName]);
    }
}