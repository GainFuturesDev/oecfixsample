﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using GF.Fix.Sample.Property;

namespace GF.Fix.Sample.Configurations
{
    internal static class Configuration
    {
        public static Props PredefinedConfiguration =>
            PropFuncs.ToProps(
                new ConfigurationParameters
                {
                    Host = "api.gainfutures.com",
                    SenderCompID = "MY_SENDER_COMPID",
                    TargetCompID = "OEC_TEST",
                    FutureAccount = "API000001",
                    ForexAccount = "APIFX0001",
                });

        public static Props AppSettingsConfiguration =>
            PropFuncs.ToProps(ConfigurationManager.AppSettings);

        public static IEnumerable<Props> MultipleTestSessions(int count) =>
            Enumerable.Range(1, count)
                .Select(no =>
                    PropFuncs.ToProps(
                        new ConfigurationParameters
                        {
                            Host = "api.gainfutures.com",
                            SenderCompID = $"TEST{no}",
                            TargetCompID = "OEC",
                            FutureAccount = "API000001",
                            ForexAccount = "APIFX0001"
                        }));
    }
    }
