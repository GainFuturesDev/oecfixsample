﻿using System;
using System.Linq;
using GF.Fix.Sample.FoxScript.Syntax;

namespace GF.Fix.Sample.FoxScript
{
    internal static class TokenParser
    {
        public static char ParseTrailingTriggerType(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new SyntaxErrorException("TrailingTriggerType not specified.");
            }
            token = token.ToUpperInvariant();
            return token.First();
        }

        public static OrderSymbol ParseOrderSymbol(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new SyntaxErrorException("OrderSymbol not specified.");
            }
            token = token.ToUpperInvariant();
            string assetPrefix = ContractAssetPrefix.ExtractFrom(ref token);

            var result = new OrderSymbol { Asset = ContractAsset.Future, Multileg = false };
            if (token.Length < 3)
            {
                result.Name = token;
            }
            else
            {
                var year = token.Substring(token.Length - 2);
                var month = token[token.Length - 3];
                var n = Tools.ContractMonths.IndexOf(month);

                if (token.Contains(',') && token.Contains('+') && token.Contains('-'))
                {
                    result.Name = token;
                    result.Multileg = true;
                    result.Asset = ContractAsset.Future;

                    if (n < 0 || !int.TryParse(year, out _))
                    {
                        result.Option = true;
                    }
                }
                else
                {
                    if (n < 0 || !int.TryParse(year, out var intYear))
                    {
                        result.Name = token;
                    }
                    else
                    {
                        result.Name = token.Substring(0, token.Length - 3);
                        result.MonthYear = Tools.CreateMonthYear(n + 1, DateTime.Now.Year / 100 * 100 + intYear);
                        result.Asset = ContractAsset.Future;
                    }
                }
            }

            if (assetPrefix != null)
            {
                result.Asset = ContractAssetPrefix.ToAsset(assetPrefix);
            }

            return result;
        }
    }
}