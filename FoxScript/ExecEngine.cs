﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using GF.Fix.Sample.Fast;
using GF.Fix.Sample.Fix;
using GF.Fix.Sample.Property;

namespace GF.Fix.Sample.FoxScript
{
    internal partial class ExecEngine
    {
        public static readonly Version FOXScriptVersion = new Version(1, 0);

        public ExecEngine(Props properties, FixEngine fixEngine, FastClient fastClient)
        {
            Properties = properties;
            _fixEngine = fixEngine;
            _fastClient = fastClient;
            _input = new StreamWriter(new MemoryStream(), Encoding.UTF8) { AutoFlush = true };
            GC.KeepAlive(new Timer(TimerFastHeartbeats, null, 25 * 1000, 25 * 1000));
        }

        public Props Properties { get; }

        public bool IsFixAvailable => _fixEngine != null;

        public bool IsFastAvailable => _fastClient != null;

        public static ExecEngine MakeFixOnly(Props properties) =>
            new ExecEngine(properties, new FixEngine(properties), null);

        public static ExecEngine MakeFastOnly(Props properties) =>
            new ExecEngine(properties, null, new FastClient(properties));

        public static ExecEngine MakeFixFast(Props properties) =>
            new ExecEngine(
                properties,
                new FixEngine(properties),
                new FastClient(properties));

        private void TimerFastHeartbeats(object state)
        {
            if (IsFastAvailable && _fastClient.Connected)
                FASTHeartbeat();
        }

        public static void Write(string format, params object[] args)
        {
            Console.Write(format, args);
        }

        public static void WriteLine(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }

        public void Execute(string line, string lineSource)
        {
            try
            {
                Parser parser = CreateParser(line, lineSource);
                parser.Parse();
            }
            catch (Exception e)
            {
                WriteLine("Execution aborted: {0}", e.Message);
            }
        }

        public void Run()
        {
            while (_running)
            {
                string line = ReadLine();
                if (string.IsNullOrWhiteSpace(line))
                    continue;

                Execute(line, "CONSOLE");
            }
        }
    }
}