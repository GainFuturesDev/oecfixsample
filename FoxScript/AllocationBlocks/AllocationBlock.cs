﻿using System;
using System.Collections.Generic;

namespace GF.Fix.Sample.FoxScript.AllocationBlocks
{
    internal class AllocationBlock<TItem>
    {
        private const int MaxNameLength = 256;

        public AllocationRule Rule;

        private readonly List<TItem> _items = new List<TItem>();
        private string _name;

        public string Name
        {
            get => string.IsNullOrEmpty(_name)
                ? string.Empty
                : _name.Substring(0, Math.Min(_name.Length, MaxNameLength));
            set => _name = value;
        }

        public IEnumerable<TItem> Items => _items;

        public void Add(TItem item)
        {
            _items.Add(item);
        }
    }
}