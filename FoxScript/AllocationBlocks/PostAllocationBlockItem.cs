﻿namespace GF.Fix.Sample.FoxScript.AllocationBlocks
{
    internal class PostAllocationBlockItem : AllocationBlockItem
    {
        public ExtendedAccount Account = new ExtendedAccount();
        public double Price;
    }
}