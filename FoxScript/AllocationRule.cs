﻿namespace GF.Fix.Sample.FoxScript
{
    internal enum AllocationRule
    {
        /// <summary>
        ///     lowest price- lowest acc
        /// </summary>
        LowAcctLowPrice = 2,

        /// <summary>
        ///     lowest price-highest acc
        /// </summary>
        LowAcctHighPrice,

        /// <summary>
        ///     highest price- lowest acc
        /// </summary>
        HighAcctLowPrice,

        /// <summary>
        ///     highest price-highest acc
        /// </summary>
        HighAcctHighPrice,

        /// <summary>
        ///     Average price system
        /// </summary>
        APS,
        PostAllocation = 1000,
        PostAllocationAPS
    }
}