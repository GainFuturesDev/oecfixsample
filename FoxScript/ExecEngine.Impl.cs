﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using GF.Fix.Sample.Fast;
using GF.Fix.Sample.Fix;
using GF.Fix.Sample.Fix.Protocols;
using GF.Fix.Sample.FoxScript.Fast;
using GF.Fix.Sample.FoxScript.Syntax;
using GF.Fix.Sample.Property;
using QuickFix;
using Object = GF.Fix.Sample.FoxScript.Syntax.Object;

namespace GF.Fix.Sample.FoxScript
{
    internal partial class ExecEngine
    {
        private readonly FastClient _fastClient;
        private readonly FixEngine _fixEngine;
        private readonly StreamWriter _input;
        private readonly Dictionary<string, MsgVar> _msgVars = new Dictionary<string, MsgVar>();
        private readonly TestStatistics _testStat = new TestStatistics();
        private bool _running = true;
        private FixProtocol _fixProtocol;

        private FixProtocol FixProtocol
        {
            get
            {
                if (_fixProtocol == null || _fixProtocol.BeginString != FixVersion.Current(Properties))
                    _fixProtocol = FixVersion.Create(Properties);
                return _fixProtocol;
            }
        }

        public ExecEngine(Props properties)
        {
            _fixProtocol = FixVersion.Create(properties);
        }

        private void AssignFields(MessageWrapper target, FixFields fields)
        {
            if (fields == null)
                return;

            foreach (FixField field in fields)
            {
                object value = GetObjectValue(field.Value, null);
                MessageWrapper.SetFieldValue(target, field.Name, value);
            }
        }

        private void ExecuteOutgoingMsgCommand(string msgVarName, OutgoingMsgCommand command)
        {
            MessageWrapper msg = CreateMessage(command);

            AssignFields(msg, command.Fields);

            SetMsgVar(msgVarName, msg);

            if (msg.IsQuickFix)
                _fixEngine.SendMessage(msg.QFMessage);
            else
            {
                var output = command as IOutputFile;
                string outputFileName = null;
                if (output != null)
                    outputFileName = output.OutputFileName;

                _fastClient.SendMessage(msg.OFMessage, outputFileName, command is CancelSubscribeCommand);
            }
        }

        private void ExecuteIncomingMsgCommand(string msgVarName, IncomingMsgCommand command)
        {
            if (command is WaitMessageCommand messageCommand)
                SetMsgVar(msgVarName, ExecuteWaitMessageCommand(messageCommand));
            else
                throw new ExecutionException("Unknown IncomingMsgCommand.");
        }

        private MessageWrapper ExecuteWaitMessageCommand(WaitMessageCommand command)
        {
            bool isQF = false;

            string msgType = IsFastAvailable ? _fastClient.GetMsgTypeValue(command.MsgTypeName) : null;
            if (msgType == null)
            {
                msgType = QFReflector.GetMsgTypeValue(command.MsgTypeName);
                isQF = true;
            }

            var timeout = Properties.ResponseTimeout;
            if (command.Timeout.HasValue)
            {
                timeout = command.Timeout.Value;
            }
            if (isQF)
            {
                var predicate = command.LogicalExpr != null
                    ? BuildMsgCtxPredicate(command.LogicalExpr)
                    : null;

                Message msg = _fixEngine.WaitMessage(msgType, timeout, predicate);
                if (msg == null)
                    FixEngine.WriteLine("	No messages received of type '{0}'", command.MsgTypeName);

                return MessageWrapper.Create(msg);
            }
            else
            {
                var predicate = command.LogicalExpr != null
                    ? BuildMsgCtxPredicateOF(command.LogicalExpr)
                    : null;

                OpenFAST.Message msg = _fastClient.WaitMessage(msgType, timeout, predicate);
                if (msg == null)
                    FixEngine.WriteLine("	No messages received of type '{0}'", command.MsgTypeName);

                return MessageWrapper.Create(msg);
            }
        }

        private object GetSyntaxConstructionValue(object constr)
        {
            if (constr == null)
                throw new SyntaxErrorException("Syntax construction not specified.");

            if (constr.IsObject())
                return GetObjectValue(constr as Object, null);

            if (constr.IsLogicalExpr())
                return BuildPredicate(constr as LogicalExpr);

            throw new SyntaxErrorException("Unknown syntax construction type '{0}'", constr.GetType().FullName);
        }

        private string ApplyFormatArgs(FormatArgs formatArgs)
        {
            if (formatArgs == null)
                return null;

            object[] args = formatArgs
                .Args
                .Select(GetSyntaxConstructionValue)
                .ToArray();

            return string.Format(formatArgs.Format, args);
        }

        public Func<bool> BuildPredicate(object logicalExpr)
        {
            BuildLogicalExpression(logicalExpr, out var body);
            return Expression.Lambda<Func<bool>>(body).Compile();
        }

        private Predicate<Message> BuildMsgCtxPredicate(object logicalExpr)
        {
            ParameterExpression context = Expression.Parameter(typeof(Message), "context");

            BuildMsgCtxLogicalExpression(logicalExpr, context, out var body);

            return Expression.Lambda<Predicate<Message>>(body, context).Compile();
        }

        private Predicate<OpenFAST.Message> BuildMsgCtxPredicateOF(object logicalExpr)
        {
            ParameterExpression context = Expression.Parameter(typeof(OpenFAST.Message), "context");

            BuildMsgCtxLogicalExpression(logicalExpr, context, out var body);

            return Expression.Lambda<Predicate<OpenFAST.Message>>(body, context).Compile();
        }

        private static Expression BuildLogicalOperation(LogicalOp op, Expression left, Expression right)
        {
            switch (op)
            {
                case LogicalOp.Or:
                    return Expression.OrElse(left, right);

                case LogicalOp.And:
                    return Expression.AndAlso(left, right);

                case LogicalOp.Equal:
                    return Expression.Call(
                        left,
                        left.Type.GetEqualsMethodInfo(),
                        Expression.Convert(right, typeof(object)));

                case LogicalOp.NotEqual:
                    MethodCallExpression expr = Expression.Call(
                        left,
                        left.Type.GetEqualsMethodInfo(),
                        Expression.Convert(right, typeof(object)));

                    return Expression.IsFalse(expr);

                case LogicalOp.Less:
                    return Expression.LessThan(left, right);

                case LogicalOp.LessOrEqual:
                    return Expression.LessThanOrEqual(left, right);

                case LogicalOp.Greater:
                    return Expression.GreaterThan(left, right);

                case LogicalOp.GreaterOrEqual:
                    return Expression.GreaterThanOrEqual(left, right);

                default:
                    throw new ExecutionException("Unknown logical operation: {0}", op);
            }
        }

        private void BuildLogicalExpression(object source, out Expression target)
        {
            if (source is LogicalExpr logicalExpr)
            {
                BuildLogicalExpression(logicalExpr.Left, out var left);
                BuildLogicalExpression(logicalExpr.Right, out var right);
                target = BuildLogicalOperation(logicalExpr.Operation, left, right);
            }
            else
            {
                if (source is Object obj)
                    target = Expression.Constant(GetObjectValue(obj, null));
                else
                    throw new ExecutionException("Unknown expression type {0}", source);
            }
        }

        private void BuildMsgCtxLogicalExpression(object source, ParameterExpression context, out Expression target)
        {
            if (source is LogicalExpr logicalExpr)
            {
                BuildMsgCtxLogicalExpression(logicalExpr.Left, context, out var left);
                BuildMsgCtxLogicalExpression(logicalExpr.Right, context, out var right);
                target = BuildLogicalOperation(logicalExpr.Operation, left, right);
            }
            else
            {
                if (source is Object obj)
                {
                    target = obj.Type == ObjectType.FixField
                        ? (Expression)Expression.Call(context.Type == typeof (Message)
                            ? QFReflector.GetFieldValueMethodInfo
                            : OFReflector.GetFieldValueMethodInfo, context, Expression.Constant(obj.Token))
                        : Expression.Constant(GetObjectValue(obj, null));
                }
                else
                {
                    throw new ExecutionException("Unknown expression type {0}", source);
                }
            }
        }

        private void SetMsgVar(string msgVarName, MessageWrapper msg)
        {
            if (string.IsNullOrEmpty(msgVarName))
                return;

            var varbl = new MsgVar(msgVarName) { Value = msg };
            _msgVars[msgVarName.ToUpperInvariant()] = varbl;
        }

        private MsgVar GetMsgVar(string msgVarName)
        {
            if (string.IsNullOrEmpty(msgVarName))
                return null;

            if (_msgVars.TryGetValue(msgVarName.ToUpperInvariant(), out var varbl))
                return varbl;

            throw new ExecutionException("Unknown message var '{0}'", msgVarName);
        }

        private MessageWrapper CreateMessage(OutgoingMsgCommand command)
        {
            MsgVar variable = null;
            if (command is OrderRefOutgoingMsgCommand orderRefCommand)
            {
                variable = GetMsgVar(orderRefCommand.OrigMsgVarName);
                variable.EnsureValueFix();
            }

            if (command is NewOrderCommand newCommand)
                return MessageWrapper.Create(FixProtocol.NewOrderSingle(newCommand));

            if (command is ModifyOrderCommand modifyCommand)
            {
                MsgVar varbl = GetMsgVar(modifyCommand.OrigMsgVarName);
                varbl.EnsureValueFix();

                return MessageWrapper.Create(FixProtocol.OrderCancelReplaceRequest(modifyCommand, varbl.Value.QFMessage));
            }

            if (command is CancelOrderCommand cancelCommand)
                return MessageWrapper.Create(FixProtocol.OrderCancelRequest(cancelCommand, variable.Value.QFMessage));

            if (command is OrderStatusCommand statusCommand)
                return MessageWrapper.Create(FixProtocol.OrderStatusRequest(statusCommand, variable.Value.QFMessage));

            if (command is OrderMassStatusCommand massStatusCommand)
                return MessageWrapper.Create(FixProtocol.OrderMassStatusRequest(massStatusCommand));

            if (command is BalanceCommand balanceCommand)
                return MessageWrapper.Create(FixProtocol.CollateralInquiry(balanceCommand));

            if (command is PositionsCommand positionsCommand)
                return MessageWrapper.Create(FixProtocol.RequestForPositions(positionsCommand));

            if (command is SymbolLookupCommand lookupCommand)
                return MessageWrapper.Create(FixProtocol.SymbolLookupRequest(lookupCommand));

            if (command is BaseContractRequestCommand baseContractCommand)
                return MessageWrapper.Create(FixProtocol.BaseContractRequest(baseContractCommand));

            if (command is ContractRequestCommand contractRequestCommand)
                return MessageWrapper.Create(FixProtocol.ContractRequest(contractRequestCommand));

            if (command is PostAllocationCommand postAllocationCommand)
                return MessageWrapper.Create(FixProtocol.AllocationInstruction(postAllocationCommand, variable.Value.QFMessage));

            if (command is UserRequestCommand userRequestCommand)
                return MessageWrapper.Create(FixProtocol.UserRequest(userRequestCommand));

            if (command is MarginCalcCommand marginCalcCommand)
                return MessageWrapper.Create(FixProtocol.MarginCalc(marginCalcCommand));

            if (command is MDMessageCommand mdMessageCommand && IsFastAvailable)
                return MessageWrapper.Create(_fastClient.MessageFactory.MarketDataRequest(mdMessageCommand));

            if (command is CancelSubscribeCommand cancelSubscribeCommand && IsFastAvailable)
            {
                MsgVar varCancel = GetMsgVar(cancelSubscribeCommand.MDMessageVar);
                varCancel.EnsureValueFAST();
                return MessageWrapper.Create(_fastClient.MessageFactory.CancelMDMessage(varCancel.Value.OFMessage));
            }

            if (command is BracketOrderCommand bracketOrderCommand)
                return MessageWrapper.Create(FixProtocol.NewOrderList(bracketOrderCommand, (msgVar, message) => SetMsgVar(msgVar, MessageWrapper.Create(message))));

            throw new ExecutionException("Unknown OutgoingMsgCommand.");
        }

        public object GetObjectValue(Object obj, MessageWrapper context)
        {
            if (obj == null)
                throw new ExecutionException("Object not specified.");

            switch (obj.Type)
            {
                case ObjectType.GlobalProp:
                    {
                        string s = obj.Token.Substring(5); //	skip "PROP:" prefix
                        return Properties.Get(s).Value;
                    }

                case ObjectType.FixConst:
                    {
                        string s = obj.Token.Substring(4); //	skip "FIX." prefix
                        int i = s.IndexOf('.');
                        return QFReflector.GetConstValue(s.Substring(0, i), s.Substring(i + 1));
                    }

                case ObjectType.FixField:
                    return MessageWrapper.GetFieldValue(context, obj.Token);

                case ObjectType.FixMsgVar:
                    return GetMsgVar(obj.Token);

                case ObjectType.FixMsgVarField:
                    {
                        int i = obj.Token.IndexOf('.');

                        MsgVar varbl = GetMsgVar(obj.Token.Substring(0, i));
                        varbl.EnsureValue();

                        return MessageWrapper.GetFieldValue(varbl.Value, obj.Token.Substring(i + 1));
                    }

                case ObjectType.Null:
                    return MsgVar.Null;

                case ObjectType.Bool:
                    return LiteralParser.ParseBool(obj.Token);

                case ObjectType.Float:
                    return LiteralParser.ParseFloat(obj.Token);

                case ObjectType.Integer:
                    return LiteralParser.ParseInteger(obj.Token);

                case ObjectType.String:
                    return LiteralParser.ParseString(obj.Token);

                case ObjectType.Timespan:
                    return LiteralParser.ParseTimespan(obj.Token);

                case ObjectType.Timestamp:
                    return LiteralParser.ParseTimestamp(obj.Token);

                case ObjectType.Date:
                    return LiteralParser.ParseDate(obj.Token);

                default:
                    throw new ExecutionException("Invalid ObjectType {0}", obj.Type);
            }
        }

        private Parser CreateParser(string script, string name)
        {
            _input.Flush();
            _input.BaseStream.SetLength(0);
            _input.WriteLine(script);

            var parser = new Parser(new Scanner(_input.BaseStream), this, new StrictErrors())
            {
                errors = { errMsgFormat = "	ERROR in [" + name + "]: {2}, Line: {0}, Col: {1}" }
            };

            return parser;
        }

        private string ReadLine()
        {
            string line = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(line))
                return line;

            return line.EndsWith(";") ? line : line + ";";
        }
    }
}