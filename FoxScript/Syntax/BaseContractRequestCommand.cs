﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class BaseContractRequestCommand : ContractRequestCommand
    {
        public CompoundType CompoundType = CompoundType.UNKNOWN;
        public string ContractGroup;
        public string Exchange;

        public string CompoundTypeString
        {
            get
            {
                Type type = typeof(CompoundType);
                string fieldValue = CompoundType.ToString();
                if (Enum.IsDefined(type, CompoundType))
                {
                    FieldInfo fieldInfo = type.GetField(fieldValue);
                    var atts = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    return atts.Length > 0 ? atts[0].Description : fieldValue;
                }
                return fieldValue;
            }
        }
    }
}