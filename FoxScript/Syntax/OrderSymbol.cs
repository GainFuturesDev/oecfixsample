﻿using System;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class OrderSymbol
    {
        public ContractAsset Asset;
        public DateTime? MonthYear;
        public bool Multileg;
        public string Name;
        public bool Option;
    }
}