﻿using System.Linq;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal static class ContractAssetPrefix
    {
        public static readonly string Future = "FUT:";
        public static readonly string Equity = "EQ:";
        public static readonly string Forex = "FX:";
        public static readonly string MutualFund = "MF:";

        public static string ExtractFrom(ref string symbol)
        {
            var prefixes = new[] { Future, Equity, Forex, MutualFund };

            string s = symbol;
            string prefix = prefixes.FirstOrDefault(s.StartsWith);

            if (prefix != null)
            {
                symbol = symbol.Substring(prefix.Length);
                return prefix;
            }
            return null;
        }

        public static ContractAsset ToAsset(string prefix)
        {
            if (prefix == Future)
            {
                return ContractAsset.Future;
            }
            if (prefix == Forex)
            {
                return ContractAsset.Forex;
            }
            throw new ExecutionException("Invalid prefix '{0}'", prefix ?? "NULL");
        }

        public static string FromAsset(ContractAsset asset)
        {
            switch (asset)
            {
                case ContractAsset.Future:
                    return Future;

                case ContractAsset.Forex:
                    return Forex;

                default:
                    throw new ExecutionException("Invalid ContractAsset.");
            }
        }
    }
}