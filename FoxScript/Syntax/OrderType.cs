﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class OrderType
    {
        public const char ICEBERG = '!';
        public const char MARKET_ON_OPEN = '@';
        public const char MARKET_ON_CLOSE = '#';

        public double? Limit;

        public int? MaxFloor;
        public double? Stop;
        public TrailingStop TrailingStop;
        public char Type;
    }
}