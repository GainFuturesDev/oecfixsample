﻿using System.Collections.Generic;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class FixFields : List<FixField>
    {
        public void Add(string name, Object value)
        {
            Add(new FixField(name, value));
        }
    }
}