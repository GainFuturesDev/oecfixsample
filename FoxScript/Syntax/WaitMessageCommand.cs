﻿using System;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class WaitMessageCommand : IncomingMsgCommand
    {
        public object LogicalExpr;
        public string MsgTypeName;
        public TimeSpan? Timeout;
    }
}