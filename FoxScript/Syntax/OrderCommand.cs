﻿using GF.Fix.Sample.FoxScript.AllocationBlocks;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal abstract class OrderCommand : OrderRequestCommand
    {
        public string Account;
        public AllocationBlock<PreAllocationBlockItem> AllocationBlock;
        public OrderContract OrderContract;
        public int OrderQty;
        public OrderSide OrderSide;
        public OrderType OrderType;
        public TimeInForce TimeInForce;
        public string TradingSession;
    }
}