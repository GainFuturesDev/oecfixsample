﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class OrderContract
    {
        public bool? Put;
        public double? Strike;
        public OrderSymbol Symbol;

        public bool Option => Put.HasValue || Strike.HasValue || Symbol.Option;
    }
}