﻿using System;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class ContractRequestCommand : OutgoingMsgCommand
    {
        public const char DEFAULT_SUBSCRIBTION_TYPE = '-';
        public string Name;
        public char SubscriptionRequestType = DEFAULT_SUBSCRIBTION_TYPE;
        public DateTime? UpdatesSinceTimestamp;
    }
}