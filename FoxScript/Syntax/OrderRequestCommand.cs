﻿using GF.Fix.Sample.FoxScript.AllocationBlocks;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal abstract class OrderRequestCommand : OutgoingMsgCommand
    {
        public string Account;
        public OrderSide OrderSide;
        public OrderContract OrderContract;
        public AllocationBlock<PreAllocationBlockItem> AllocationBlock;
    }
}