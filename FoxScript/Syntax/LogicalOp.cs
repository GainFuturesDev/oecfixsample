﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal enum LogicalOp
    {
        Or,
        And,
        Equal,
        NotEqual,
        Less,
        LessOrEqual,
        Greater,
        GreaterOrEqual
    }
}