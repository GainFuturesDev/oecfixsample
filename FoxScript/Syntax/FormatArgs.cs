﻿using System.Collections.Generic;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class FormatArgs
    {
        public readonly string Format;
        private readonly List<object> _args = new List<object>();

        public FormatArgs(string format)
        {
            Format = format;
        }

        public IEnumerable<object> Args => _args;

        public void AddArg(object arg)
        {
            if (arg == null)
            {
                return;
            }
            _args.Add(arg);
        }
    }
}