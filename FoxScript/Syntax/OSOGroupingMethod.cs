﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    public enum OsoGroupingMethod
    {
        None = -1,
        ByFirstPrice,
        ByPrice,
        ByFill
    }
}