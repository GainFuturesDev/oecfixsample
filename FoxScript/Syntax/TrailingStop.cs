﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class TrailingStop
    {
        public double? Amount;
        public bool AmountInPercents;
        public char? TriggerType;
        public double? RefPrice;
        public double? Delta;
    }
}