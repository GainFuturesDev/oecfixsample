﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class FixField
    {
        public readonly string Name;
        public readonly Object Value;

        public FixField(string name, Object value)
        {
            Name = name;
            Value = value;
        }
    }
}