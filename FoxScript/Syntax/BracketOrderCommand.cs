﻿using System.Collections.Generic;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class BracketOrderCommand : OutgoingMsgCommand
    {
        public BracketType Type;

        public OsoGroupingMethod OsoGroupingMethod = OsoGroupingMethod.None;

        public List<BracketCommandItem> BracketCommands;
    }
}