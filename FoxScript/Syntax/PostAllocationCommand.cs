﻿using GF.Fix.Sample.FoxScript.AllocationBlocks;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class PostAllocationCommand : OrderRefOutgoingMsgCommand
    {
        public AllocationBlock<PostAllocationBlockItem> AllocationBlock;
        public OrderContract Contract;
    }
}