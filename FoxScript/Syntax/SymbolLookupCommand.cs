﻿using System.Collections.Generic;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class SymbolLookupCommand : BaseContractRequestCommand
    {
        public string BaseContract;
        public bool? ByBaseContractsOnly;
        public List<ContractKind> ContractKinds = new List<ContractKind>();

        public ContractType? ContractType;
        public int MaxRecords;
        public int Mode;
        public OptionType OptionType = OptionType.ALL;
        public bool? OptionsRequired;

        public Contract ParentContract;
        public string SearchText;
    }
}