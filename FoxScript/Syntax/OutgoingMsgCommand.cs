﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal abstract class OutgoingMsgCommand : MsgCommand
    {
        public readonly FixFields Fields = new FixFields();
    }
}