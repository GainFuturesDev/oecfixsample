﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class Object
    {
        public readonly string Token;
        public readonly ObjectType Type;

        public Object(ObjectType type, string token)
        {
            Type = type;
            Token = token;
        }

        public override string ToString()
        {
            return $"{Type}: {Token ?? "NULL"}";
        }
    }
}