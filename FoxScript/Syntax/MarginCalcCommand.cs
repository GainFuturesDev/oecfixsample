﻿using System.Collections.Generic;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class MarginCalcCommand : OutgoingMsgCommand
    {
        public string Account;
        public List<Position> Positions;

        public class Position
        {
            public OrderContract Contract;
            public int MaxQty;
            public int MinQty;
        }
    }
}