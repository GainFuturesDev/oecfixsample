﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class UserRequestCommand : OutgoingMsgCommand
    {
        public string Name;
        public string UUID;
        public int UserRequestType = QuickFix.Fields.UserRequestType.REQUEST_INDIVIDUAL_USER_STATUS;
    }
}