﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class LogicalExpr
    {
        public object Left;
        public LogicalOp Operation;
        public object Right;
    }
}