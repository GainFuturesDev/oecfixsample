﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal enum ContractRequestType
    {
        Request,
        Lookup
    }
}