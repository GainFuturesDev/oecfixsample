﻿using System;

namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal class TimeInForce
    {
        public DateTime? Expiration;
        public char Type;
    }
}