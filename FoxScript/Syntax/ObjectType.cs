﻿namespace GF.Fix.Sample.FoxScript.Syntax
{
    internal enum ObjectType
    {
        Null,
        Integer,
        Float,
        String,
        Bool,
        Timestamp,
        Date,
        Timespan,

        FixMsgVar,
        FixMsgVarField,
        FixField,
        FixConst,
        GlobalProp
    }
}