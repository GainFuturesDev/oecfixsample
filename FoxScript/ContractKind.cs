﻿namespace GF.Fix.Sample.FoxScript
{
    internal enum ContractKind
    {
        UNKNOWN,
        FUTURE = 1,
        OPTION = 3,
        FOREX = 5,
        CONTINUOUS = 11,
        GENERIC_COMPOUND = 256,
        FUTURE_COMPOUND = 257,
        OPTIONS_COMPOUND = 258,
        CUSTOM_COMPOUND = 259
    }
}