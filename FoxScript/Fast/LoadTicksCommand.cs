﻿using System;

namespace GF.Fix.Sample.FoxScript.Fast
{
    internal class LoadTicksCommand : SubscribeTicksCommand
    {
        private DateTime? _endTime;
        private TimeSpan? _endTimeSpan;

        public override int SubscriptionRequestType => 0;

        public override DateTime? EndTime
        {
            get
            {
                if (_endTime.HasValue)
                    return _endTime;

                DateTime now = DateTime.UtcNow;
                if (_endTimeSpan.HasValue)
                    return now - _endTimeSpan.Value;
                return now;
            }
        }

        public void SetEndTime(DateTime endTime)
        {
            _endTime = endTime;
        }

        public void SetEndTime(TimeSpan endTimeSpan)
        {
            _endTimeSpan = endTimeSpan;
        }
    }
}