﻿using System;
using System.Collections.Generic;
using GF.Fix.Sample.Fast;

namespace GF.Fix.Sample.FoxScript.Fast
{
    internal class SubscribeTicksCommand : MDMessageCommand
    {
        private DateTime? _startTime;
        private TimeSpan? _startTimeSpan;

        public override int MarketDepth => 1;

        public override int SubscriptionType => 2;

        public override DateTime? StartTime
        {
            get
            {
                if (_startTime.HasValue)
                {
                    return _startTime;
                }
                DateTime now = DateTime.UtcNow;
                if (_startTimeSpan.HasValue)
                {
                    return now - _startTimeSpan.Value;
                }
                return now;
            }
        }

        public void SetStartTime(DateTime startTime)
        {
            _startTime = startTime;
        }

        public void SetStartTime(TimeSpan startTimeSpan)
        {
            _startTimeSpan = startTimeSpan;
        }

        public SubscribeTicksCommand()
        {
            Entries = new List<MDEntryType>(new[] { MDEntryType.BID, MDEntryType.OFFER, MDEntryType.TRADE });
        }
    }
}