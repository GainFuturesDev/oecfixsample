﻿using System;
using System.Collections.Generic;
using GF.Fix.Sample.Fast;

namespace GF.Fix.Sample.FoxScript.Fast
{
    internal class SubscribeHistogramCommand : MDMessageCommand
    {
        public override int MarketDepth => 0;

        public override int SubscriptionType => 4;

        public override DateTime? StartTime => null;

        public SubscribeHistogramCommand()
        {
            Entries = new List<MDEntryType>(new[] { MDEntryType.TRADE });
        }
    }
}