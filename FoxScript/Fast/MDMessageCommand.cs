﻿using System;
using System.Collections.Generic;
using GF.Fix.Sample.Fast;
using GF.Fix.Sample.FoxScript.Syntax;

namespace GF.Fix.Sample.FoxScript.Fast
{
    internal abstract class MDMessageCommand : OutgoingMsgCommand, IOutputFile
    {
        protected List<MDEntryType> Entries;

        public string BaseSymbol;
        public int? ExpirationMonth;
        public ContractKind ContractKind;

        protected MDMessageCommand()
        {
            UpdateType = 1;
            Entries = new List<MDEntryType>();
        }

        public int UpdateType { get; set; }

        public FastStrikeSide StrikeSide { get; set; }

        public bool Option => StrikeSide != null;

        public abstract int MarketDepth { get; }

        public abstract int SubscriptionType { get; }

        public virtual int SubscriptionRequestType => 1;

        public MDEntryType[] MDEntries => Entries.ToArray();

        public abstract DateTime? StartTime { get; }

        public virtual DateTime? EndTime => null;

        public string OutputFileName { get; set; }

        public void Add(MDEntryType type)
        {
            Entries.Add(type);
        }

        public void ResetMDEntries()
        {
            Entries.Clear();
        }
    }
}