﻿using GF.Fix.Sample.FoxScript.Syntax;

namespace GF.Fix.Sample.FoxScript.Fast
{
    internal class CancelSubscribeCommand : OutgoingMsgCommand
    {
        public string MDMessageVar;
    }
}