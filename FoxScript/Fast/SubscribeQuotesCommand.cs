﻿using System;
using System.Collections.Generic;
using GF.Fix.Sample.Fast;

namespace GF.Fix.Sample.FoxScript.Fast
{
    internal class SubscribeQuotesCommand : MDMessageCommand
    {
        public override int MarketDepth => 1;

        public override int SubscriptionType => 0;

        public override DateTime? StartTime => null;

        public SubscribeQuotesCommand()
        {
            Entries = new List<MDEntryType>(
                new[]
                {
                    MDEntryType.BID,
                    MDEntryType.OFFER,
                    MDEntryType.TRADE,
                    MDEntryType.OPENING_PRICE,
                    MDEntryType.SETTLEMENT_PRICE,
                    MDEntryType.TRADE_VOLUME,
                    MDEntryType.OPEN_INTEREST
                }
            );
        }
    }
}