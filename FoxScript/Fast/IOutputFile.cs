﻿namespace GF.Fix.Sample.FoxScript.Fast
{
    internal interface IOutputFile
    {
        string OutputFileName { get; set; }
    }
}