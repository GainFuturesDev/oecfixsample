﻿using System;
using System.Collections.Generic;
using GF.Fix.Sample.Fast;

namespace GF.Fix.Sample.FoxScript.Fast
{
    internal class SubscribeDomCommand : MDMessageCommand
    {
        public override int MarketDepth => 0;

        public override int SubscriptionType => 1;

        public override DateTime? StartTime => null;

        public SubscribeDomCommand()
        {
            Entries = new List<MDEntryType>(new[] { MDEntryType.BID, MDEntryType.OFFER });
        }
    }
}