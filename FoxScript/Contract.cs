﻿using GF.Fix.Sample.Cfi;

namespace GF.Fix.Sample.FoxScript
{
    internal class Contract
    {
        public Code Code;
        public MaturityMonthYear MaturityMonthYear;
        public double? Strike;
        public string Symbol;
    }
}