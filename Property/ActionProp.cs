﻿using System;

namespace GF.Fix.Sample.Property
{
    internal class ActionProp<T> : Prop
    {
        private readonly Func<T> _getter;
        private readonly Action<T> _setter;

        public ActionProp(string name, Func<T> getter, Action<T> setter)
            : base(typeof(T), name)
        {
            _getter = getter ?? throw new ArgumentNullException(nameof(getter));
            _setter = setter ?? throw new ArgumentNullException(nameof(setter));
        }

        protected override object GetValue()
        {
            return _getter();
        }

        protected override void SetValue(object value)
        {
            _setter((T)value);
        }
    }
}