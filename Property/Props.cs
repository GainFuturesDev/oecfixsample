﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GF.Fix.Sample.Property
{
    internal class Props : IEnumerable<Prop>
    {
        private readonly Dictionary<string, Prop> _props = new Dictionary<string, Prop>();

        public string UUID
        {
            get => GetString(Prop.UUID);
            set => SetCreating(Prop.UUID, value);
        }

        public string Host
        {
            get => GetString(Prop.Host);
            set => SetCreating(Prop.Host, value);
        }

        public int Port
        {
            get => GetInt(Prop.Port);
            set => SetCreating(Prop.Port, value);
        }

        public string SenderCompID
        {
            get => GetString(Prop.SenderCompID);
            set => SetCreating(Prop.SenderCompID, value);
        }

        public string TargetCompID
        {
            get => GetString(Prop.TargetCompID);
            set => SetCreating(Prop.TargetCompID, value);
        }

        public string FastHashCode
        {
            get => GetString(Prop.FastHashCode);
            set => SetCreating(Prop.FastHashCode, value);
        }

        public bool ResetSeqNumbers
        {
            get => Get<bool>(Prop.ResetSeqNumbers);
            set => SetCreating(Prop.ResetSeqNumbers, value);
        }

        public int SenderSeqNum
        {
            get => GetInt(Prop.SenderSeqNum);
            set => SetCreating(Prop.SenderSeqNum, value);
        }

        public int TargetSeqNum
        {
            get => GetInt(Prop.TargetSeqNum);
            set => SetCreating(Prop.TargetSeqNum, value);
        }

        public int FastPort
        {
            get => GetInt(Prop.FastPort);
            set => SetCreating(Prop.FastPort, value);
        }

        public int HeartbeatInterval
        {
            get => GetInt(Prop.HeartbeatInterval);
            set => SetCreating(Prop.HeartbeatInterval, value);
        }

        public int ReconnectInterval
        {
            get => GetInt(Prop.ReconnectInterval);
            set => SetCreating(Prop.ReconnectInterval, value);
        }

        public bool MillisecondsInTimestamp
        {
            get => GetBool(Prop.MillisecondsInTimestamp);
            set => SetCreating(Prop.MillisecondsInTimestamp, value);
        }

        public string BeginString
        {
            get => GetString(Prop.BeginString);
            set => SetCreating(Prop.BeginString, value);
        }

        public int LogonTimeout
        {
            get => GetInt(Prop.LogonTimeout);
            set => SetCreating(Prop.LogonTimeout, value);
        }

        public TimeSpan SessionStart
        {
            get => GetTimeSpan(Prop.SessionStart);
            set => SetCreating(Prop.SessionStart, value);
        }

        public TimeSpan SessionEnd
        {
            get => GetTimeSpan(Prop.SessionEnd);
            set => SetCreating(Prop.SessionEnd, value);
        }

        public bool SSL
        {
            get => GetBool(Prop.SSL);
            set => SetCreating(Prop.SSL, value);
        }

        public string Password
        {
            get => GetString(Prop.Password);
            set => SetCreating(Prop.Password, value);
        }

        public TimeSpan ConnectTimeout
        {
            get => GetTimeSpan(Prop.ConnectTimeout);
            set => SetCreating(Prop.ConnectTimeout, value);
        }

        public string ForexAccount
        {
            get => GetString(Prop.ForexAccount);
            set => SetCreating(Prop.ForexAccount, value);
        }

        public string FutureAccount
        {
            get => GetString(Prop.FutureAccount);
            set => SetCreating(Prop.FutureAccount, value);
        }

        public TimeSpan ResponseTimeout
        {
            get => Get<TimeSpan>(Prop.ResponseTimeout);
            set => SetCreating(Prop.ResponseTimeout, value);
        }

        private bool GetBool(string name) =>
            Get<bool>(name);

        private string GetString(string name) =>
            Get<string>(name);

        private int GetInt(string name) =>
            Get<int>(name);

        private TimeSpan GetTimeSpan(string name) =>
            Get<TimeSpan>(name);

        private T Get<T>(string name) =>
            (T)Get(name).Value;

        public Prop Get(string name)
        {
            if (!_props.TryGetValue(name.ToUpperInvariant(), out var prop))
                throw new ExecutionException("Property '{0}' not found.", name);
            return prop;
        }

        public void SetCreating<T>(string name, T value)
        {
            if (_props.TryGetValue(name.ToUpperInvariant(), out var prop))
                prop.Value = value;
            else
                AddProp(name, value);
        }

        #region IEnumerable<Prop> Members

        public IEnumerator<Prop> GetEnumerator()
        {
            return _props.Values.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public void AddProp<T>(string name, Func<T> getter, Action<T> setter)
        {
            _props[name.ToUpperInvariant()] = new ActionProp<T>(name, getter, setter);
        }

        public void AddProp<T>(string name, T value)
        {
            _props[name.ToUpperInvariant()] = new ValueProp<T>(name, value);
        }

        public bool Contains(string name) =>
            _props.ContainsKey(name.ToUpperInvariant());
    }
}