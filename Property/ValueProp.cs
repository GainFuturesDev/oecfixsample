﻿namespace GF.Fix.Sample.Property
{
    internal class ValueProp<T> : Prop
    {
        private T _value;

        public ValueProp(string name, T value)
            : base(typeof(T), name)
        {
            _value = value;
        }

        protected override object GetValue()
        {
            return _value;
        }

        protected override void SetValue(object value)
        {
            _value = (T)value;
        }
    }
}