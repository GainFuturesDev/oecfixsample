﻿using System;

namespace GF.Fix.Sample.Property
{
    internal abstract class Prop
    {
        public static readonly string Host = "Host";
        public static readonly string Port = "Port";
        public static readonly string FastPort = "FastPort";
        public static readonly string ReconnectInterval = "ReconnectInterval";
        public static readonly string HeartbeatInterval = "HeartbeatInterval";
        public static readonly string MillisecondsInTimestamp = "MillisecondsInTimestamp";
        public static readonly string SessionStart = "SessionStart";
        public static readonly string SessionEnd = "SessionEnd";
        public static readonly string BeginString = "BeginString";
        public static readonly string SenderCompID = "SenderCompID";
        public static readonly string TargetCompID = "TargetCompID";
        public static readonly string SenderSeqNum = "SenderSeqNum";
        public static readonly string TargetSeqNum = "TargetSeqNum";
        public static readonly string ResponseTimeout = "ResponseTimeout";
        public static readonly string ConnectTimeout = "ConnectTimeout";
        public static readonly string FutureAccount = "FutureAccount";
        public static readonly string ForexAccount = "ForexAccount";
        public static readonly string FastHashCode = "FastHashCode";
        public static readonly string ResetSeqNumbers = "ResetSeqNumbers";
        public static readonly string LogonTimeout = "LogonTimeout";
        public static readonly string SSL = "SSL";
        public static readonly string Password = "Password";
        public static readonly string UUID = "UUID";
        public readonly string Name;
        public readonly Type Type;

        protected Prop(Type type, string name)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            Type = type;
            Name = name;
        }

        public object Value
        {
            get => GetValue();
            set
            {
                if (value == null)
                    throw new ExecutionException("Assigning property '{0}' to NULL.", Name);
                if (value.GetType() != Type)
                    throw new ExecutionException("Invalid value type '{0}' for property '{1}'.", value.GetType(), Name);

                SetValue(value);
            }
        }

        public void Parse(string value)
        {
            SetValue(Convert.ChangeType(value, Type));
        }

        protected abstract object GetValue();

        protected abstract void SetValue(object value);
    }
}