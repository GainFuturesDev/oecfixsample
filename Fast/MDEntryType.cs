﻿namespace GF.Fix.Sample.Fast
{
    public enum MDEntryType
    {
        BID = '0',
        OFFER = '1',
        TRADE = '2',
        OPENING_PRICE = '4',
        SETTLEMENT_PRICE = '6',
        TRADE_VOLUME = 'B',
        OPEN_INTEREST = 'C',
        WORKUP_TRADE = 'w',
        EMPTY_BOOK = 'J'
    }
}