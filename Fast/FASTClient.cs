﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading;
using GF.Fix.Sample.Property;
using OpenFAST;
using OpenFAST.Error;
using OpenFAST.Sessions;
using OpenFAST.Sessions.Tcp;
using OpenFAST.Template;
using OpenFAST.Template.Loader;

namespace GF.Fix.Sample.Fast
{
    internal class FastClient
    {
        private const string LocalFilename = ".\\templates.xml";

        private readonly ClientMessageHandler _clientMessageHandler;
        private readonly SessionControlProtocol11 _protocol;
        private readonly ITemplateRegistry _templateRegistry;
        private OpenFAST.Sessions.FastClient _fastClient;
        private Session _session;
        private readonly Props _properties;

        public FastClient(Props properties)
        {
            _properties = properties;

            var templateLoader = new XmlMessageTemplateLoader { LoadTemplateIdFromAuxId = true };

            TryDownload();

            var template = File.Exists(LocalFilename)
                ? new FileStream(LocalFilename, FileMode.Open, FileAccess.Read)
                : Assembly.GetExecutingAssembly().GetManifestResourceStream("GF.Fix.Sample.template.template.xml");

            templateLoader.Load(template);
            _templateRegistry = templateLoader.TemplateRegistry;
            MessageFactory = new FastMessageFactory(_templateRegistry);
            _clientMessageHandler = new ClientMessageHandler();
            _protocol = new SessionControlProtocol11();
            _protocol.RegisterSessionTemplates(_templateRegistry);
        }

        public FastMessageFactory MessageFactory { get; }

        private void TryDownload()
        {
            if (File.Exists(LocalFilename))
                return;

            try
            {
                var client = new WebClient();
                client.DownloadFile("http://api.gainfutures.com/Sections/Misc/DownloadFile.aspx?ClientUpdate=0_5008_1", LocalFilename);
            }
            catch (Exception)
            {
            }
        }

        public bool Connected => _session != null && _session.IsListening;

        public void Connect(string username, string password)
        {
            int port = _properties.Contains(Prop.FastPort)
                ? _properties.FastPort
                : _properties.Port;

            _fastClient = new OpenFAST.Sessions.FastClient("client", _protocol, new TcpEndpoint(_properties.Host, port))
            {
                InboundTemplateRegistry = _templateRegistry,
                OutboundTemplateRegistry = _templateRegistry
            };

            _session = _fastClient.Connect();
            _session.ErrorHandler = new ClientErrorHandler();
            _session.MessageHandler = _clientMessageHandler;
            // OpenFAST has a bug where FastClient ignores the ITemplateRegistry given to it. So we register them here.
            _session.MessageOutputStream.TemplateRegistry.RegisterAll(_templateRegistry);
            _session.MessageInputStream.TemplateRegistry.RegisterAll(_templateRegistry);

            SendMessage(MessageFactory.LogonMessage(username, password));
        }

        private void EnsureConnected()
        {
            if (_session == null || !_session.IsListening)
                throw new ExecutionException("Not connected to FAST server.");
        }

        public void SendMessage(Message message) =>
            SendMessage(message, null, false);

        public void SendMessage(Message message, string outputFileName, bool isCancel)
        {
            EnsureConnected();
            if (!string.IsNullOrWhiteSpace(outputFileName))
                _clientMessageHandler.AddOutputFiles(message.GetString(FastFieldsNames.MDREQID), outputFileName);

            _session.MessageOutputStream.WriteMessage(message);

            if (isCancel)
                _clientMessageHandler.RemoveOutputFiles(message.GetString(FastFieldsNames.MDREQID));

            Console.WriteLine("<fast out>: {0}", message);
        }

        public Message WaitMessage(string msgType, TimeSpan timeout, Predicate<Message> predicate)
        {
            EnsureConnected();
            return _clientMessageHandler.WaitMessage(msgType, timeout, predicate);
        }

        internal void SendHeartbeat()
        {
            SendMessage(MessageFactory.Heartbeat());
        }

        public string GetMsgTypeValue(string templateName)
        {
            if (_templateRegistry.TryGetTemplate(templateName, out var template))
            {
                if (template.HasField(FastFieldsNames.MSGTYPE))
                {
                    Field tf = template.GetField(FastFieldsNames.MSGTYPE);
                    return (tf as Scalar)?.DefaultValue.ToString();
                }
            }

            return null;
        }

        public void Disconnect()
        {
            if (_session != null && _session.IsListening)
            {
                _session.MessageOutputStream.WriteMessage(_protocol.CloseMessage);
                int step = 0;
                while (_session.IsListening && step < 20)
                {
                    Thread.Sleep(100);
                    step++;
                }
                _session.Close();
                _clientMessageHandler.Clear();
                Console.WriteLine("FAST: Disconnect");
            }
            else
            {
                Console.WriteLine("FAST: : Already disconnected from FAST server.");
            }
        }

        #region Nested type: ClientErrorHandler

        private class ClientErrorHandler : IErrorHandler
        {
            public void OnError(Exception exception, StaticError error, string format, params object[] args)
            {
                if (format != null)
                    Console.WriteLine(format, args);
                else
                    Console.WriteLine(error);
            }

            public void OnError(Exception exception, DynError error, string format, params object[] args)
            {
                if (format != null)
                    Console.WriteLine(format, args);
                else
                    Console.WriteLine(error);
            }

            public void OnError(Exception exception, RepError error, string format, params object[] args)
            {
                if (format != null)
                    Console.WriteLine(format, args);
                else
                    Console.WriteLine(error);
            }
        }

        #endregion
    }
}