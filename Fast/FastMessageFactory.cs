﻿using System;
using GF.Fix.Sample.FoxScript;
using GF.Fix.Sample.FoxScript.Fast;
using OpenFAST;
using OpenFAST.Template;
using Message = OpenFAST.Message;

namespace GF.Fix.Sample.Fast
{
    internal class FastMessageFactory
    {
        private readonly ITemplateRegistry _templateRegistry;
        private int _counter = 1;

        public FastMessageFactory(ITemplateRegistry templateRegistry)
        {
            _templateRegistry = templateRegistry;
        }

        public Message CancelMDMessage(Message msg)
        {
            if (!msg.IsDefined(FastFieldsNames.MDREQID))
                throw new ExecutionException("FAST message not contains '{0}'", FastFieldsNames.MDREQID);

            string mdReqID = msg.GetString(FastFieldsNames.MDREQID);
            _templateRegistry.TryGetTemplate("MarketDataRequest_Cancel", out MessageTemplate template);
            var message = new Message(template);
            message.SetString(FastFieldsNames.MDREQID, mdReqID);
            return message;
        }

        public Message MarketDataRequest(MDMessageCommand cmd)
        {
            _templateRegistry.TryGetTemplate("MarketDataRequest", out MessageTemplate template);

            string symbolStr = $"{cmd.BaseSymbol}{cmd.ExpirationMonth}";
            if (cmd.Option)
                symbolStr += $"{(cmd.StrikeSide.Put ? "P" : "C")}{cmd.StrikeSide.Strike}";

            var message = new Message(template);
            message.SetString("MDReqID", $"{symbolStr}_{cmd.SubscriptionType}_{++_counter}");
            message.SetInteger("SubscriptionRequestType", cmd.SubscriptionRequestType);
            message.SetInteger("MarketDepth", cmd.MarketDepth);
            message.SetInteger("MDUpdateType", cmd.UpdateType);
            message.SetInteger("SubscriptionType", cmd.SubscriptionType);

            DateTime? startTime = cmd.StartTime;
            if (startTime.HasValue)
            {
                long st = OFReflector.ToFastDateTime(startTime.Value);
                message.SetLong("StartTime", st);
            }

            DateTime? endTime = cmd.EndTime;
            if (endTime.HasValue)
            {
                long st = OFReflector.ToFastDateTime(endTime.Value);
                message.SetLong("EndTime", st);
            }

            Sequence templateMDEntries = template.GetSequence("MDEntries");
            var mdEntries = new SequenceValue(templateMDEntries);

            foreach (MDEntryType mdentryType in cmd.MDEntries)
            {
                var mdEntry = new GroupValue(templateMDEntries.Group);
                mdEntry.SetString("MDEntryType", ((char)mdentryType).ToString());
                mdEntries.Add(mdEntry);
            }

            message.SetFieldValue("MDEntries", mdEntries);

            Sequence templateInstrument = template.GetSequence("Instruments");
            var groupInstrument = new GroupValue(templateInstrument.Group);
            groupInstrument.SetString("Symbol", cmd.BaseSymbol);

            string cfiCode = "FXXXXS";
            switch (cmd.ContractKind)
            {
                case ContractKind.OPTION:
                    cfiCode = cmd.StrikeSide.Put ? "OPXFXS" : "OCXFXS";
                    groupInstrument.SetDecimal("StrikePrice", (decimal)cmd.StrikeSide.Strike);
                    break;
                case ContractKind.FOREX:
                    cfiCode = "ERXXXN";
                    break;
                case ContractKind.FUTURE_COMPOUND:
                    cfiCode = "FMXXXN";
                    break;
                case ContractKind.OPTIONS_COMPOUND:
                    cfiCode = "OMXFXN";
                    break;
            }
            groupInstrument.SetString("CFICode", cfiCode);

            if (cmd.ExpirationMonth.HasValue)
                groupInstrument.SetInteger("MaturityMonthYear", cmd.ExpirationMonth.Value);

            var mdInstruments = new SequenceValue(templateInstrument);
            mdInstruments.Add(groupInstrument);
            message.SetFieldValue("Instruments", mdInstruments);
            return message;
        }

        public Message LogonMessage(string username, string password)
        {
            if (!_templateRegistry.TryGetTemplate("Logon", out var template))
                throw new Exception("Template Logon not registered");

            var message = new Message(template);
            message.SetLong("SendingTime", OFReflector.ToFastDateTime(DateTime.UtcNow));
            message.SetInteger("HeartbeatInt", 30);
            message.SetString("Username", username);
            message.SetString("Password", password);
            return message;
        }

        public Message Heartbeat()
        {
            if (!_templateRegistry.TryGetTemplate("Heartbeat", out var template))
                throw new Exception("Template Heartbeat not registered");

            var message = new Message(template);
            message.SetLong("SendingTime", OFReflector.ToFastDateTime(DateTime.UtcNow));
            return message;
        }
    }
}