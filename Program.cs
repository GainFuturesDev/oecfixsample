﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using GF.Fix.Sample.Configurations;
using GF.Fix.Sample.FoxScript;

namespace GF.Fix.Sample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("FIX Sample Client, ver. {0}", Assembly.GetExecutingAssembly().GetName().Version);
            Console.WriteLine("FOXScript ver. {0}", ExecEngine.FOXScriptVersion);
            PrintAuthUsage();

            Start();
            //            StartAppSettings();
            //            StartMultiple();
        }

        //Ordinary way to start. FIX and FAST enabled; console available
        private static void Start() =>
            Start(ExecEngine.MakeFixFast(
                Configuration.PredefinedConfiguration),
                StartConsoleRoutine);

        // Settings from config file; console
        private static void StartAppSettings() =>
            Start(
                ExecEngine.MakeFixFast(Configuration.AppSettingsConfiguration),
                StartConsoleRoutine);

        // Connects multiple fixengines simultaneously; no command console
        private static void StartMultiple() =>
            Start(
                Configuration.MultipleTestSessions(100).Select(ExecEngine.MakeFixOnly),
                TestMultipleConnectionsRoutine);

        private static void Start(IEnumerable<ExecEngine> engines, Action<int, ExecEngine> routine)
        {
            var threads = engines.Select((engine, i) =>
            {
                void ThreadRoutine(ExecEngine e) => routine(i, e);
                return new Thread(() => Start(engine, ThreadRoutine));
            }).ToArray();

            foreach (var thread in threads)
                thread.Start();
            foreach (var thread in threads)
                thread.Join();
        }

        private static void Start(ExecEngine execEngine, Action<ExecEngine> routine)
        {
            execEngine.LoadSeqNumbers();
            routine(execEngine);
            execEngine.StoreSeqNumbers();
        }

        private static void TestMultipleConnectionsRoutine(int no, ExecEngine engine)
        {
            const string source = "COMMAND";
            engine.Execute($"connect '{no + 1}';", source);
            engine.Execute("sleep [00:03:00];", source);
            engine.Execute("disconnect;", source);
        }

        private static void MultipleFastConnection(int no, ExecEngine engine)
        {
            const string source = "COMMAND";
            if (engine.IsFixAvailable)
                engine.Execute($"connect '{no + 1}';", source);

            const int fixDelay = 5;
            const int orderDelay = 3;
            TimeSpan delay = TimeSpan.FromSeconds(fixDelay + orderDelay * no);
            var sleepString = $"sleep [{delay}];";

            if (engine.IsFixAvailable)
                engine.Execute("disconnect;", source);

            engine.Execute(sleepString, source);
            engine.Execute("ConnectFast;", source);
        }

        private static void ExecuteFileRoutine(ExecEngine engine, string fileName)
        {
            string command = $"exec '{fileName}'";
            engine.Execute(command, "COMMAND");
        }

        private static void StartConsoleRoutine(ExecEngine engine)
        {
            engine.Run();
        }

        private static void PrintAuthUsage()
        {
            const string fileName = "AuthWorkflow.fox";
            const string fullFileName = "../../Tests/" + fileName;
            if (!File.Exists(fullFileName)) return;

            Console.WriteLine();
            Console.WriteLine("Hint of day from: " + fileName);
            Console.WriteLine();
            using (StreamReader file = File.OpenText(fullFileName))
                Console.Write(file.ReadToEnd());
            Console.WriteLine();
        }
    }
}